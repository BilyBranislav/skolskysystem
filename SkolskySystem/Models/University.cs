﻿using System.Collections.Generic;

namespace SkolskySystem.Models
{
    public class University
    {

        public int UniversityId { get; set; }
        public string UniversityName { get; set; }
        public List<Student> Students { get; set; }
    }
}
