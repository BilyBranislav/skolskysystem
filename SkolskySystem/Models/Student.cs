﻿namespace SkolskySystem.Models
{
    public class Student
    {

        public int StudentId { get; set; }
        public string FistName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }

        public int UniversityId { get; set; }
        public University University { get; set; }
    }
}
