﻿using System;
using System.Collections.Generic;
using SkolskySystem.Models;

namespace SkolskySystem.Dtos
{
    public class UniversityDto
    {

        public string UniName { get; set; }


        public University ToDb()
        {
            if (string.IsNullOrWhiteSpace(UniName)) return null;
            return new University
            {
                UniversityName = UniName
            };
        }
    }
}
