﻿using System;
using Microsoft.AspNetCore.Mvc;
using SkolskySystem.Dtos;
using SkolskySystem.Models;

namespace SkolskySystem.Controllers
{
    [ApiController]
    [Route("api/uni")]
    public class UniversityController : ControllerBase
    {
        private readonly UniversityContext _db;

        public UniversityController(UniversityContext db)
        {
            _db = db;
        }

        [HttpPost]
        [Route("create")]
        public IActionResult CreateUni([FromBody] UniversityDto university)
        {

            var dbModel = university.ToDb();
            if(dbModel == null)
            {
                return BadRequest();
            } 
            else
            {
                _db.Universities.Add(dbModel);
                _db.SaveChanges();
                return Ok();
            }
        }
    }
}
